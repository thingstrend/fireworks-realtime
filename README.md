# Fireworks!

[![Fireworks](http://lab.aerotwist.com/canvas/fireworks/capture.png)](http://lab.aerotwist.com/canvas/fireworks/)

In honor of July 4th I made a small Node.JS realtimeapp that allows users to launch fireworks together. See [demo](http://brightideasfilms.com/fireworks/).

Instructions:

1.Install [NodeJs](http://nodejs.org/download/)

2.Clone this repo:


```
#!bash

git clone https://bitbucket.org/all3nt/fireworks-realtime
```

3.Next navigate into the directory and install socket.io

```
#!bash
cd fireworks-realtime
npm install socket.io
```

4.Move the html,js,css and sound files to your web server (yes I know I am not using NodeJs for this part).

5.Update the socket.io link and the socket.io connect urls in index.html and client.js

6.Then in the fireworks-realtime run:


```
#!bash

node fireworks-server.js
```

7.Navigate to the index.html in your web server and bam your done.

Sources:

[Fireworks Particles Tutorial](http://creativejs.com/tutorials/creating-fireworks/)

[Sound Effects](http://www.mysoundeffect.com/backgrounds/)