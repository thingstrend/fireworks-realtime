var socket = io.connect('http://localhost:8000');

socket.on('shootFirework', function(username){
		Fireworks.createParticle();
		$("#username__" + username).find('span').removeClass('hideFirework');
		setTimeout(function(){
			$("#username__" + username).find('span').addClass('hideFirework');
		}, 5000);
});

var my_username;
socket.on('myUsername', function(uname){
	my_username = uname;
});

socket.on('chat', function(username, mess, the_date){
	$("#chats").prepend('<div><div class="a_date">' + the_date + '</div>' + username + ' : ' + mess + '</div><br />');
});

var clients = [];
socket.on('updateUsers', function(usernames){
	var data = '';

	for(var x in usernames){
		if(clients.indexOf(usernames[x]) == -1){
			clients.push(usernames[x]);
			$("#members").append('<li class="bg-primary" id="username__' + usernames[x] + '"><span class="glyphicon glyphicon-asterisk hideFirework"></span>' + usernames[x] + '</li>');
			$("#username__"+ usernames[x]).fadeIn();
		}
	}
	for(var x in clients){
		if(usernames.indexOf(clients[x]) == -1){
			clients.splice(clients[x], 1);
			$("#username__" + clients[x]).fadeOut().remove();
		}
	}
	$("#countOnline").html('(' + clients.length +')');
});


function showOthersFirework(){
	socket.emit('shootFirework');
	Fireworks.createParticle();
}
/*
	Begin the fun and submit their username
 */

function launch(){
	var uname = $("#uname").val();
	if(uname != ''){
			socket.emit('adduser', uname);
		$("#enter").hide();
		$("#launchFirework").show();
		$("#license").show();
		$("#lobbyContainer").show();
		$("#launchFirework").on('mouseup', showOthersFirework);
		$("#launchFirework").on('touchend', showOthersFirework);
	}
	else{
		alert('Please Enter A Valid Username');
	}


}

function send(){
	var d = new Date();
	var the_date = d.getMonth() + '/' + d.getDate() + '/' + d.getFullYear() + ' at ' + d.getHours() + ':' + d.getMinutes();
	var mess = ($("#chatText").val()).replace(/(<([^>]+)>)/ig,"");
	$("#chatText").val('');//reset
	$("#chats").prepend('<div class="myChat"><div class="a_date">' + the_date + '</div>' + my_username + ' : ' + mess + '</div><br />');
	socket.emit('chat', mess, the_date);
}

$(document).ready(function(){
	if($.browser.msie || /MSIE 9/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)){
		console.log('iam ie');
		$(".badBrowser").show();
	}
		
});