/*
 * @Author - Allen Taylor
 * @description  - Node.js server for fireworks
 */

// Require dependencies

var usernames = [];
var app = require('http').createServer()
, fs = require('fs')
, io = require('socket.io').listen(app);

//http://stackoverflow.com/questions/19156636/node-js-and-socket-io-creating-room

app.listen(8000);


/*
	Initial Connection
 */
io.sockets.on('connection', function(socket) {
	//get username
	 socket.on('adduser', function (username){
	 	console.log(username);
	 	if(usernames.indexOf(username) == -1){
 			socket.username = username;
 			usernames.push(username);
	 	}
	 	else{
	 		test_username = username;
	 		var i = 0;
	 		//assign the user an appropriate username
	 		while(usernames.indexOf(test_username) != -1){
	 			test_username = username + '_' + i;
	 			i++;
	 		}
	 		username = test_username;
	 		socket.username = username;
 			usernames.push(username);
	 	}
	 	console.log('emitting?');
	 	// new version
	 	socket.emit('myUsername', socket.username);
	 	////old version
	 	 // io.sockets.socket(socket.id).emit('myUsername', socket.username);
	 	io.sockets.emit('updateUsers', usernames);
	 	//tell chat new user has joined
	 	var d = new Date();
	 	var the_date = d.getMonth() + '/' + d.getDate() + '/' + d.getFullYear() + ' at ' + d.getHours() + ':' + d.getMinutes();
	 	socket.broadcast.emit('chat', 'server', socket.username + ' has joined fireworks.', the_date);
	 });

	 //on emitted shoot firework emit shoot firework
	 socket.on('shootFirework', function(){
	 	socket.broadcast.emit('shootFirework', socket.username);
	 });

	 //on emitted shoot firework emit shoot firework
	 socket.on('chat', function(mess, the_date){
	 	socket.broadcast.emit('chat', socket.username, mess, the_date);
	 });

	 //when the connection terminates
	 socket.on('disconnect', function(){
	 	//tell chat new user has joined
	 	var d = new Date();
	 	var the_date = d.getMonth() + '/' + d.getDate() + '/' + d.getFullYear() + ' at ' + d.getHours() + ':' + d.getMinutes();
	 	socket.broadcast.emit('chat', 'server', socket.username + ' has left fireworks.', the_date);
	 	for(x in usernames){
	 		if(usernames[x] == socket.username)
	 			usernames.splice(x, 1);//remove from array
	 	}
	 	io.sockets.emit('updateUsers', usernames);//update remnaining users
	 });
});

/*
Socket.io examples:
// send to current request socket client
 socket.emit('message', "this is a test");

 // sending to all clients, include sender
 io.sockets.emit('message', "this is a test");

 // sending to all clients except sender
 socket.broadcast.emit('message', "this is a test");

 // sending to all clients in 'game' room(channel) except sender
 socket.broadcast.to('game').emit('message', 'nice game');

  // sending to all clients in 'game' room(channel), include sender
 io.sockets.in('game').emit('message', 'cool game');

 // sending to individual socketid
 io.sockets.socket(socketid).emit('message', 'for your eyes only');
 */
 